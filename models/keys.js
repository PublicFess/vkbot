var mongoose = require('mongoose')
  , Schema = mongoose.Schema;

var keySchema = Schema({
  groupId: {
    type: 'Number',
    require: true
  },
  response: {
    type: 'String',
    require: true
  }
});

var User = mongoose.model('Key', keySchema);
module.exports = User;
