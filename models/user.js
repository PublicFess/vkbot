var mongoose = require('mongoose')
  , Schema = mongoose.Schema;

var userSchema = Schema({
  name: {
    type: 'String',
    require: true
  },
  chatId: {
    type: 'Number',
    required: true
  },
  groupIds: [{
    name: 'String',
    groupId: 'Number'
  }]
});

var User = mongoose.model('User', userSchema);
module.exports = User;
