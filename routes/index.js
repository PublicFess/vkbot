var mongoose = require('mongoose')
  , User = require('../models/user')
  , Keys = require('../models/keys');

var findGroup = function(groups, id) {
  var group = null;
  groups.forEach(function(g) {
    if (g.groupId == id) {
      group = g;
    }
  });
  return group;
};

mongoose.Promise = global.Promise;

module.exports = function(app) {
  app.post('/callback', function(req, res) {
    var body = req.body;
    var type = body.type;
    if (type == 'confirmation') {
      Keys.findOne({
        groupId: parseInt(body.group_id)
      }).exec().then(function(key) {
        return res.send(key.response);
      });
    } else {

      User.find({
        groupIds: {
          $elemMatch: {
            groupId: body.group_id
          }
        }
      }).exec().then(function(users) {
        users.forEach(function (user) {
          var group = findGroup(user.groupIds, body.group_id);
          if (type == 'wall_post_new') {
            global.tg.sendMessage(user.chatId, '' +
              + '' + group.name + '\n' +
              'Новое сообщение на стене.\n' +
              'http://vk.com/wall-' + body.group_id + '\n' + body.object.text);
          }

          if (type == 'wall_reply_new') {
            global.tg.sendMessage(user.chatId, '' +
              + '' + group.name + '\n' +
              'Новый комментарий к записи на стене.\n' +
              'http://vk.com/wall-' + body.group_id + '_' + body.object.post_id + '\n' + body.object.text);
          }

          if (type == 'group_join') {
            global.tg.sendMessage(user.chatId, '' +
              + '' + group.name + '\n' +
              '!!!Новый пользователь присоединился к группе!!!\n' +
              'http://vk.com/id' + body.object.user_id);
          }

          if (type == 'group_leave') {
            global.tg.sendMessage(user.chatId, '' +
              + '' + group.name + '\n' +
              '!!!Пользователь вышел из группы!!!\n' +
              'http://vk.com/id' + body.object.user_id);
          }
        });
        return res.send('ok');
      });
    }
  });
};
