var mongoose = require('mongoose')
  , User = require('../models/user');

mongoose.Promise = global.Promise;
module.exports = function($) {
  User.findOne({
    chatId: $.user.id
  }).exec().then(function(user) {
    if (user) {
      return user;
    }
    return {};
  }).then(function(result) {
    if (result.chatId) {
      $.sendMessage('Я знаю тебя, ' + result.name + '.');
      return;
    }
    var user = new User({
      chatId: $.user.id,
      name: $.user.first_name
    });
    return user.save();
  }).then(function(user) {
    if (!user) {
      return;
    }
    $.sendMessage('Привет '+ user.name + '. Для добавления группы воспользуйся командой /add');
  }).catch(function(err) {
    console.log(err);
  });
};
