var mongoose = require('mongoose')
  , User = require('../models/user');

var form = {
  groupId: {
    q: 'Напиши id группы',
    error: 'sorry, wrong input',
    validator: function(input, callback) {
      callback(true);
    }
  },
  groupTitle: {
    q: 'Напиши название группы',
    error: 'sorry, wrong input',
    validator: function(input, callback) {
      callback(true);
    }
  }
};

mongoose.Promise = global.Promise;
module.exports = function($) {
  var baseUser;
  User.findOne({
    chatId: $.user.id
  }).exec().then(function(user) {
    if (!user) {
      throw new Error('Для начала надо познакомиться. Используй /start для этого.');
    }
    baseUser = user;
    return
  }).then(function() {
    return new Promise(function(resolve) {
      $.runForm(form, function(result) {
        resolve(result);
      });
    });
  }).then(function(result) {
    baseUser.groupIds.push({
      name: result.groupTitle,
      groupId: parseInt(result.groupId)
    });
    return baseUser.save();
  }).then(function() {
    $.sendMessage('Теперь я буду рассказывать тебе о твоей группе.');
  }).catch(function(err) {
    $.sendMessage(err);
  });
};
