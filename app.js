var express = require('express')
  , bodyParser = require('body-parser')
  , app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header('Access-Control-Allow-Credentials', true);
  res.header('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

var tg = require('telegram-node-bot')('164070773:AAE-ahD5XHD75pFPV006uQMwZ_Og_VEaag8');

var startCtrl = require('./controllers/start.js');
var addCtrl = require('./controllers/add.js');
tg.controller('StartController', startCtrl);
tg.controller('AddController', addCtrl);

tg.router
    .when(['start'], 'StartController')
    .when(['add'], 'AddController');

global.tg = tg;

require('./routes/index')(app);

var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/vkbot');

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  app.listen(19000, function () {
    console.log('Example app listening on port 19000!');
  });
});
